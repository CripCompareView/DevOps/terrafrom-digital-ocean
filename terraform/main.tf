terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
    }
  }
}

variable "digitalocean_token" {
  type        = string
  default     = ""
  description = "Digital Ocen token"
}

provider "digitalocean" {
  token = var.digitalocean_token
}

resource "digitalocean_droplet" "tf-CripCompare-dev" {
  image  = "ubuntu-20-04-x64"
  name   = "tf-CripCompare-dev"
  size   = "s-1vcpu-1gb"
  region = "lon1"
  tags   = ["docker", "docker-swarm", "docker-swarm-manager"]
}
